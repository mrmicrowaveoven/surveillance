SURVEIL = False
VIDEO_LENGTH_IN_SECS = 60

#
# Capture video for 1 hour
# Send video to AWS
# - Name of file will be chateau-front-door-#######
# -- # is timestamp
# Delete local video
# Repeat above

import requests

import boto3

import os

import time

import subprocess

import threading

from picamera import PiCamera

camera = PiCamera()
camera.resolution = (640,480)

last_log = time.time()

def surveil():
	while True:
		time_stamp = time.strftime("~%Y_%m_%d~%H_%M_%S", time.localtime())
		file_name = 'dirk-surveillance' + time_stamp + '.h264'
		print_timestamp("Recording " + file_name + "...")

		record_video(file_name)
		print_timestamp("Recording complete.  Summoning daemon...")

		x = threading.Thread(target=convert_upload_and_delete_video, args=([file_name]))
		x.start()
		print_timestamp("Daemon summoned.  Beginning next recording...")

def print_timestamp(details):
	global last_log
	print(details)
	print(time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime()))
	# print(last_log)
	seconds_since_last_log = int(time.time() - last_log)
	last_log = time.time()
	print("That took " + str(seconds_since_last_log) + " seconds.")
	print("\n")

def convert_upload_and_delete_video(file_name):
	print_timestamp("WHO HATH SUMMONED ME.  I WILL MANAGE " + file_name)

	mp4_file = make_mp4(file_name)
	print_timestamp("MP4 Conversion is complete.  Uploading to cloud...")

	send_video(mp4_file)
	print_timestamp("Upload complete.  Deleting videos...")

	delete_video(file_name)
	delete_video(mp4_file)
	print_timestamp("Videos deleted.  MY WORK IS DONE.")

def record_video(file_name):
	camera.start_recording(file_name)
	camera.wait_recording(VIDEO_LENGTH_IN_SECS)
	camera.stop_recording()

def make_mp4(file_name):
	mp4_file_name = make_mp4_file_name(file_name)
	subprocess.call(["MP4Box", "-add", file_name, mp4_file_name])
	return(mp4_file_name)

def make_mp4_file_name(file_name):
	length = len(file_name)
	return(file_name[:length - 5] + '.mp4')

def delete_video(file_name):
	os.remove(file_name)

def send_video(file_name):
	s3_client = boto3.client('s3')
	bucket_name = 'dirk-surveillance'
	s3_client.upload_file(file_name, bucket_name, 'alley/' + file_name)

if SURVEIL:
	surveil()
